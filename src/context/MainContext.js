import React, { useState, createContext } from 'react';

export const MainContext = createContext();

const MainProvider = ({ children }) => {
  const [currentAccount, setCurrentAccount] = useState([]);
  const [currentContact, setCurrentContact] = useState([]);
  const [contacts, setContacts] = useState([]);

  return (
    <MainContext.Provider value={{
      currentAccount,
      setCurrentAccount,
      currentContact,
      setCurrentContact,
      contacts,
      setContacts
    }}
    >
      {children}
    </MainContext.Provider>
  );
};
export default MainProvider;
