import React from 'react'
import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Navbar from './components/navbar/Navbar';
import AccountsTable from './components/accounts-table/AccountsTable';
import AccountPage from './components/account-page/AccountPage';
import ContactPage from './components/ContactPage/ContactPage';

function App() {
  return (
    <div>
      <BrowserRouter>
        <Navbar/>
        <Routes>
          <Route path="/" element={<AccountsTable />} />
          <Route path="/details" element={<AccountPage />} />
          <Route path="/contact" element={<ContactPage />} />
        </Routes>
        
      </BrowserRouter>
    </div>
  );
}

export default App;
