import React, { useState, useContext } from 'react';
import MaterialTable from 'material-table';
import { useNavigate } from 'react-router-dom';
//import fetchAccounts from './AccountsTableService';
import Constants from '../../utilities/constants';
import { MainContext } from '../../context/MainContext';
import styles from './AccountsTable.module.css'
import { apiJson } from '../../utilities/demo'

const AccountsTable = () => {

  const response = JSON.parse(apiJson);
  const accounts = response.records;

  const [apiError, setApiError] = useState(false);
  const {
    setCurrentAccount,
    setContacts
  }  = useContext(MainContext);

  const navigate = useNavigate();

  const handleClick = async (event, rowData) => {
    if (event != null) {
      await setCurrentAccount({
        Id: rowData.id,
        Name: rowData.name,
        AnnualRevenue: rowData.annualRevenue,
        Website: rowData.website,
        AccountNumber: rowData.accountNumber,
        Rating: rowData.rating,
        UpsellOpportunity__c: rowData.upsell
      });      
      await setContacts(rowData.contacts);
      navigate("/details", {replace: true});
    }
  };

  const columns = [
    {
      title: 'Account ID', field: 'id', hidden:true
    },
    {
      title: 'Account Name', field: 'name', defaultSort: 'asc'
    },
    {
      title: 'Annual Revenue', field: 'annualRevenue'
    },
    {
      title: 'Website', field: 'website'
    },
    {
      title: 'Account Number', field: 'accountNumber'
    },
    {
      title: 'Rating', field: 'rating'
    },
    {
      title: 'Upsell', field: 'upsell'
    },
    {
      title: 'Contacts', field: 'contacts', hidden:true
    }
  ];

  return (
    <>
    {apiError
        ? <p className={styles.error}>{Constants.API_ERROR}</p>
        : (
          <MaterialTable
          title="My Accounts"
            columns={columns}

            data = {accounts.map((account) => (
              {
                id: account.Id,
                name: account.Name,
                annualRevenue: account.AnnualRevenue,
                website: account.Website,
                accountNumber: account.AccountNumber,
                rating: account.Rating,
                upsell: account.UpsellOpportunity__c,
                contacts: account.Contacts.records
              }
            ))}
            onRowClick={(event, rowData) => handleClick(event, rowData)}
            options={{
              headerStyle: {
                backgroundColor: '#3f51b5',
                color: '#FFF'
              },
              pageSize: 8,
              pageSizeOptions: [8, 15, 30],
              paginationType: 'stepped',
              search: true,
              tableLayout: "fixed",
              columnResizable: false
            }}
          />
        )}
    </>
  );
};
export default AccountsTable;