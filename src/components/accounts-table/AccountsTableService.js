import HttpHelper from '../../utilities/HttpHelper';
import Constants from '../../utilities/constants';

/**
 *
 * @name fetchAccountss
 * @description Utilizes HttpHelper to make a get request to an API
 * @param {*} setAccounts sets state for accounts
 * @param {*} setApiError sets error if response other than 200 is returned
 * @returns sets state for products if 200 response, else sets state for apiError
 */
export default async function fetchAccounts(setAccounts, setApiError) {
  await HttpHelper(Constants.ACCOUNTS_ENDPOINT, 'GET')
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error(Constants.API_ERROR);
    })
    .then(setAccounts)
    .catch(() => {
      setApiError(true);
    });
}
