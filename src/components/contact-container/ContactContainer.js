import React, { useContext } from 'react';
import MaterialTable from 'material-table';
import { useNavigate } from 'react-router-dom';
import { MainContext } from '../../context/MainContext';
import styles from './ContactContainer.module.css'

const ContactContainer = () => {

  const { contacts, setCurrentContact } = useContext(MainContext);

  const navigate = useNavigate();

  const columns = [
    {
      title: 'Contact ID', field: 'id', hidden: true
    },
    {
      title: 'Name', field: 'name', defaultSort: 'asc'
    },
    {
      title: 'Title', field: 'title'
    },
    {
      title: 'Phone', field: 'phone'
    },
    {
      title: 'Department', field: 'department'
    },
    {
      title: 'Email', field: 'email'
    }
  ];
  const handleClick = async (event, rowData) => {
    if (event != null) {
      await setCurrentContact({
        Id: rowData.id,
        Name: rowData.name,
        Title: rowData.title,
        Phone: rowData.phone,
        Department: rowData.department,
        Email: rowData.email
      });
      navigate("/contact", {replace: true});
    }
  };

  return (
    <div className={styles.main}>
      <div className={styles.body}>
        <MaterialTable
          className={styles.table}
          title="Account Contacts"
          columns={columns}
          data={contacts.map((contact) => (
            {
              id: contact.Id,
              name: contact.Name,
              title: contact.Title,
              phone: contact.Phone,
              department: contact.Department,
              email: contact.Email
            }
          ))}
          onRowClick={(event, rowData) => handleClick(event, rowData)}
          options={{
            headerStyle: {
              backgroundColor: '#3f51b5',
              color: '#FFF'
            },
            pageSize: 8,
            pageSizeOptions: [8, 15, 30],
            paginationType: 'stepped',
            search: true
          }}
        />
      </div>
    </div>
  );
};
export default ContactContainer;