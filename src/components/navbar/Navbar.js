import React from 'react';
import { Link } from 'react-router-dom';
import styles from './Navbar.module.css';
import logo from '../../utilities/logo.png';

/**
 * @name Navbar
 * @returns navigation bar component
 */
const Navbar = () => (
  <div className={styles.navbar}>
    <div />
    <p className={styles.navbarTitle}>ACE PRODUCTS - ACCOUNT MANAGEMENT</p>
    <div className={styles.logoPosition}>
      <Link to="/"><img src={logo} className={styles.logo} alt="logo" /></Link>
    </div>
  </div>
);
export default Navbar;
