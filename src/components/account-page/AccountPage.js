import React, { useContext } from "react";
import { Link } from 'react-router-dom';
import {
  Button, TextField
} from '@material-ui/core';
import { NavigateBefore } from '@material-ui/icons';
import styles from './AccountPage.module.css';
import { MainContext } from '../../context/MainContext';
import ContactContainer from "../contact-container/ContactContainer";

const AccountPage = () => {
  const {
    currentAccount, contacts
  } = useContext(MainContext);
  console.log(currentAccount);

  return (
    <div>
      <div className={styles.main}>
        <div className={styles.body}>
          <div className={styles.heading}>
            <span className={styles.backButton}>
              <Link to="/">
                <Button
                  color="primary"
                  startIcon={<NavigateBefore />}
                  style={{ width: 80 }}
                  variant="contained"
                >
                  BACK
                </Button>
              </Link>
            </span>
            <span>
              Account Details
            </span>
          </div>
          <form>
            <div className={styles.inputTop}>
              <TextField
                className={styles.text}
                defaultValue={currentAccount.Id}
                inputProps={{
                  readOnly: true
                }}
                label="Account ID"
                variant="outlined"
              />
              <TextField
                className={styles.text}
                defaultValue={currentAccount.Name}
                inputProps={{
                  readOnly: true
                }}
                label="Account Name"
                variant="outlined"
              />
            </div>
            <div className={styles.input}>
            <TextField
                className={styles.text}
                defaultValue={currentAccount.AnnualRevenue}
                inputProps={{
                  readOnly: true
                }}
                label="Annual Revenue"
                variant="outlined"
              />
              <TextField
                className={styles.text}
                defaultValue={currentAccount.Website}
                inputProps={{
                  readOnly: true
                }}
                label="Website"
                variant="outlined"
              />
            </div>
            <div className={styles.input}>
            <TextField
                className={styles.text}
                defaultValue={currentAccount.AccountNumber}
                inputProps={{
                  readOnly: true
                }}
                label="Account Number"
                variant="outlined"
              />  
              <TextField
                className={styles.text}
                defaultValue={currentAccount.Rating}
                inputProps={{
                  readOnly: true
                }}
                label="Rating"
                variant="outlined"
              />
              </div>
              <div className={styles.input}>
                
              <TextField
                className={styles.text}
                defaultValue={currentAccount.UpsellOpportunity__c}
                inputProps={{
                  readOnly: true
                }}
                label="Upsell Opportunity"
                variant="outlined"
            />            
            </div>
          </form>
        </div>
      </div>
      <div className={styles.main}>
        <div className={styles.body}>
          <div className={styles.contactTable}>
            <ContactContainer
              contacts={contacts}
            />
          </div>
        </div>
      </div>
    </div>
  );
  
};
export default AccountPage;