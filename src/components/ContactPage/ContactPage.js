import React, { useContext } from "react";
import { Link } from 'react-router-dom';
import {
  Button, TextField
} from '@material-ui/core';
import { NavigateBefore } from '@material-ui/icons';
import styles from './ContactPage.module.css';
import { MainContext } from '../../context/MainContext';

const ContactPage = () => {
  const {
    currentContact
  } = useContext(MainContext);

  return (
    <div>
      <div className={styles.main}>
        <div className={styles.body}>
          <div className={styles.heading}>
            <span className={styles.backButton}>
              <Link to="/details">
                <Button
                  color="primary"
                  startIcon={<NavigateBefore />}
                  style={{ width: 80 }}
                  variant="contained"
                >
                  BACK
                </Button>
              </Link>
            </span>
            <span>
              Contact Details
            </span>
          </div>
          <form>
            <div className={styles.inputTop}>
              <TextField
                className={styles.text}
                defaultValue={currentContact.Id}
                inputProps={{
                  readOnly: true
                }}
                label="Contact ID"
                variant="outlined"
              />
              <TextField
                className={styles.text}
                defaultValue={currentContact.Name}
                inputProps={{
                  readOnly: true
                }}
                label="Name"
                variant="outlined"
              />
            </div>
            <div className={styles.input}>
            <TextField
                className={styles.text}
                defaultValue={currentContact.Title}
                inputProps={{
                  readOnly: true
                }}
                label="Title"
                variant="outlined"
              />
              <TextField
                className={styles.text}
                defaultValue={currentContact.Phone}
                inputProps={{
                  readOnly: true
                }}
                label="Phone"
                variant="outlined"
              />
            </div>
            <div className={styles.input}>
            <TextField
                className={styles.text}
                defaultValue={currentContact.Department}
                inputProps={{
                  readOnly: true
                }}
                label="Department"
                variant="outlined"
              />  
              <TextField
                className={styles.text}
                defaultValue={currentContact.Email}
                inputProps={{
                  readOnly: true
                }}
                label="Email"
                variant="outlined"
              />
              </div>
              <div className={styles.input}>
            </div>
          </form>
        </div>
      </div>
      
    </div>
  );
  
};
export default ContactPage;