module.exports = Object.freeze({
  // fetch constants
  API_ERROR: 'Oops, something went wrong',
  BASE_URL_UI: 'http://localhost:8000/',
  BASE_URL_API: 'http://localhost:3000',
  ACCOUNTS_ENDPOINT: '/accounts'
});
